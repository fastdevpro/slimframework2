<?php 
include '../config.php';
/**
* Пример <? inView('index','header');?>
* $folder = index
* $value = header
*  Пример :	<? inView($folder,$value);?>
* Пример1 :	<? inView('index','header');?>
* Пример2 : ../view/$folder/layout/$value
* Пример3 : ../view/index/layout/header.php
*/
function inView($folder,$value)
{
	return include '../view/'.$folder.'/'.'layout/'.$value.'.php';
}

function in($value)
{
	return include $value;
}


// Подключение файлов css //
function css($folder = 'css',$file)
{
	echo '<link rel="stylesheet" href="'.$folder.'/'.$file.'">
	';
}

// Подключение файлов js //
function src($value)
{
	echo '<script src="'.$value.'"></script>
	';
}


function title()
{
	echo title;
}
function keywords()
{
	echo keywords;
}
function description()
{
	echo description;
}

function viewport()
{
	echo viewport;
}