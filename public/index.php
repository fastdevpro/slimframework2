<?php
require '../vendor/autoload.php';

use \Slim\Slim;

Slim::registerAutoloader();

$app = new Slim();

require '../connect.php';
require '../routes/web.php';

$app->run();
