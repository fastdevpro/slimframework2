<?php 

$app->get('/', function () use($app) 
	{
		$app->etag('unique-id');
		$app->render('../../view/index/index.php');
	}
);


$app->get('/signup', function () use($app) 
	{
		$app->etag('unique-id');
		$app->render('../../view/signup/index.php');
	}
);


$app->get('/signin', function () use($app) 
	{
		$app->etag('unique-id');
		$app->render('../../view/signin/index.php');
	}
);
