<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><? title(); ?></title>

	<meta name="keywords" content="<? keywords(); ?>">
	<meta name="description" content="<? description(); ?>">
	<meta name="viewport" content="<? viewport(); ?>">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<?
	css('css','bootstrap-reboot.css');
	css('css','bootstrap-grid.min.css');
	css('css','main.css');
	css('css','start.css');

	src('https://code.jquery.com/jquery-3.2.1.js') 
	?>
</head>
